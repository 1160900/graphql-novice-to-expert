const express = require('express');
const graphqlHTTP = require('express-graphql');
const schema = require('./schema/schema');
const mongoose =require('mongoose');
const cors =require('cors');
const app = express();

app.use(cors());
mongoose.connect('mongodb://1160900:teste123@ds129536.mlab.com:29536/graphqi-ninja');
mongoose.connection.once('open', ()=>{
    console.log('connected to db');
})
app.use('/graphql', graphqlHTTP({

    schema,
    graphiql : true
}));
app.listen(4000, () => {
    console.log('listening for request on port 4000');
});